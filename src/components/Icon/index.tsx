import './styles.scss';

import React from 'react';
import IconsSVG from 'src/assets/images/svg/icons.svg';

type Props = {
    name: string;
    size: number;
};

const Icon = ({ name, size }: Props): React.ReactElement => {
    return (
        <svg className="icon" width={size} height={size}>
            {/* eslint-disable-next-line @typescript-eslint/restrict-template-expressions */}
            <use xlinkHref={`${IconsSVG}#icon-${name}`} />
        </svg>
    );
};

export default Icon;
