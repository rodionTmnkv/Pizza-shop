//Импорт стилей
import './styles.sass';

import React, { PropsWithChildren } from 'react';
import ReactModal from 'react-modal';

//Импорт компонентов
import Icon from '../Icon';

ReactModal.setAppElement('#root');

type Props = {
    isOpen: boolean;
    closeModal: () => void;
    title: string;
};

const Modal = ({
    isOpen,
    closeModal,
    title,
    children,
}: PropsWithChildren<Props>): React.ReactElement => {
    return (
        <ReactModal isOpen={isOpen} onRequestClose={closeModal} className="modal_wrapper">
            <div className="modal_window">
                <div className="modal_title">
                    <h3>{title}</h3>
                    <button className="btn_close" onClick={closeModal}>
                        <Icon name="close" size={20} />
                    </button>
                </div>

                {children}
            </div>
        </ReactModal>
    );
};

export default Modal;
