import React, { useEffect, useState } from 'react';

//Импорт Компонентов
import MainStocks from '../../template/Main/modules/MainStocks';
import StocksSlider from '../StocksSlider';

type TState = {
    width: number | undefined;
    height: number | undefined;
};

const Stocks = (): React.ReactElement => {
    const [windowSize, setWindowSize] = useState<TState>({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {
        function handleResize() {
            setWindowSize({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        }

        window.addEventListener('resize', handleResize);

        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowSize.width && windowSize.width < 772 ? <StocksSlider /> : <MainStocks />;
};

export default Stocks;
