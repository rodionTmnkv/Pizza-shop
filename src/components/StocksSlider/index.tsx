// Импорт стилей
import './styles.sass';

import React from 'react';
import Slider from 'react-slick';

import { Images } from '../../utils/images';

function StocksSlider(): React.ReactElement {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
    };

    return (
        <div>
            <Slider {...settings}>
                <div>
                    <img src={Images.MainStockPizza} alt="" />
                    <h1>текст1</h1>
                </div>
                <div>
                    <img src={Images.MainStockDrink} alt="" />
                    <h1>текст2</h1>
                </div>
                <div>
                    <img src={Images.MainStockDiscount} alt="" />
                    <h1>текст3</h1>
                </div>
            </Slider>
        </div>
    );
}

export default StocksSlider;
