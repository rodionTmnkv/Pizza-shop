// Импорт стилей
import './assets/styles/site.sass';

import React, { useState } from 'react';

import { Cart } from './modals/Cart';
import Footer from './template/Footer';
// Импорт компонентов
import Header from './template/Header';
import Main from './template/Main';

function App(): React.ReactElement {
    const [needShowCart, setNeedShowCart] = useState(false);

    return (
        <>
            <Cart isOpen={needShowCart} onClose={() => setNeedShowCart(false)} />

            <Header showCart={() => setNeedShowCart(true)} />
            <Main />
            <Footer />
        </>
    );
}

export default App;
