import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import { store } from './mobx/store';
import { Provider } from 'mobx-react';

const rootElement = document.getElementById('root');

if (rootElement) {
    const root = createRoot(rootElement);
    root.render(
        <Provider {...store}>
            <React.StrictMode>
                <App />
            </React.StrictMode>
        </Provider>
    );
}
