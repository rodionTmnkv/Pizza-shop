import React from 'react';
import { inject, observer } from 'mobx-react';
import { IStore } from '../mobx/store';

export const withStateManager = <P, I extends Partial<IStore>>(
    component: (props: P & I) => React.ReactElement,
    injectPart: (stores: IStore) => I
) => {
    return inject(injectPart)(observer(component)) as unknown as (props: P) => React.ReactElement;
};
