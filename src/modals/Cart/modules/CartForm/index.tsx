//Импорт стилей
import './styles.sass';

import React from 'react';
import { useForm } from 'react-hook-form';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

type FormData = {
    name: string;
    phone: string;
    address: string;
    payment: '' | 'courier' | 'online';
};

type InjectedProps = Pick<IStore, 'cart'>;

type Props = {
    onClose: () => void;
};

const CartForm = ({ onClose, cart }: Props & InjectedProps): React.ReactElement => {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm<FormData>({
        defaultValues: {
            payment: '',
        },
    });

    const onSubmit = (data: FormData) => {
        console.log('FormData: ', data, 'Items: ', cart.items);
        cart.clearCart();
        onClose();
    };

    return (
        <form className="form" onSubmit={() => handleSubmit(onSubmit)}>
            <p className="default-text">Контакты</p>
            <div className="form-contacts">
                <div className="form-contacts__name">
                    <input
                        className="form-input"
                        placeholder="Ваше имя"
                        {...register('name', {
                            required: true,
                            pattern: /^[A-Za-zА-Яа-яЁё]+$/i,
                        })}
                    />
                    {errors.name && (
                        <span className="form-input__errors">
                            Имя должно состоять из букв!{errors.name.message}
                        </span>
                    )}
                </div>

                <div className="form-contacts__phone">
                    <input
                        className="form-input"
                        placeholder="Телефон"
                        {...register('phone', {
                            required: true,
                            pattern: /^[0-9]+(\.[0-9]{1,2})?$/,
                        })}
                    />
                    {errors.phone && (
                        <span className="form-input__errors">
                            Номер должен состоять из цифр!{errors.phone.message}
                        </span>
                    )}
                </div>

                <div className="form-contacts__address">
                    <input
                        className="form-input"
                        placeholder="Адрес доставки"
                        {...register('address', {
                            required: {
                                value: true,
                                message: 'Поле обязательно для заполнения!',
                            },
                        })}
                    />
                    {errors.address && (
                        <span className="form-input__errors">{errors.address.message}</span>
                    )}
                </div>
            </div>
            <p className="default-text">Способ оплаты</p>
            <div className="form-payment">
                <label className="form-input__container" htmlFor="courier">
                    <input
                        className="form-input__radiobutton"
                        id="courier"
                        type={'radio'}
                        value={'courier'}
                        {...register('payment', {
                            required: {
                                value: true,
                                message: 'Поле обязательно для заполнения!',
                            },
                        })}
                    />
                    <span className="form-input__container__text">
                        Оплата наличными или картой курьеру
                    </span>
                </label>

                <label className="form-input__container" htmlFor="offline">
                    <input
                        className="form-input__radiobutton"
                        type={'radio'}
                        value={'offline'}
                        {...register('payment', {
                            required: {
                                value: true,
                                message: 'Выберите тип оплаты!',
                            },
                        })}
                    />
                    <span className="form-input__container__text">
                        Оплата картой онлайн на сайте
                    </span>
                    {errors.payment && <span>{errors.payment.message}</span>}
                </label>
            </div>

            <button className="form-button" type={'submit'}>
                Оформить заказ
            </button>

            <p className="description">
                Нажимая кнопку «Оформить заказ» вы соглашаетесь с политикой конфиденциальности
            </p>
        </form>
    );
};

export default withStateManager<Props, InjectedProps>(CartForm, stores => ({
    cart: stores.cart,
}));
