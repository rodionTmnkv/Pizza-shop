import React from 'react';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

//Импорт компонентов
import Icon from '../../../../components/Icon';

//Импорт стилей
import './styles.sass';

type InjectedProps = Pick<IStore, 'cart'>;

const CartItems = ({ cart }: InjectedProps): React.ReactElement => {
    return (
        <div className="carts-container">
            <ul className="carts-container__list">
                {cart.items.map(item => (
                    <li key={`${item.pizza.id}_${item.size}`}>
                        <div className="carts-container__info">
                            <div className="carts-container__card__image">
                                {item.pizza.types.map(type => (
                                    <img key={type.id} src={type.image} alt={type.name} />
                                ))}
                            </div>
                            <img
                                className="carts-container__info__image"
                                src={item.pizza.image}
                                alt={item.pizza.name}
                            />
                            <div className="carts-container__info__title">
                                <p className="default-text">{item.pizza.name}</p>
                                <p className="description">{item.size} см</p>
                            </div>
                        </div>
                        <div className="carts-container__target">
                            <button
                                className="carts-container__target__delete"
                                disabled={item.count === 1}
                                onClick={() => cart.decreaseCount(item.pizza.id, item.size)}
                            >
                                <Icon name="deletepizza" size={24} />
                            </button>

                            <span className="carts-container__count">{item.count}</span>

                            <button
                                className="carts-container__target__add"
                                onClick={() => cart.increaseCount(item.pizza.id, item.size)}
                            >
                                <Icon name="addpizza" size={24} />
                            </button>
                        </div>
                        <span className="carts-container__item__summary">
                            {' '}
                            {item.count * item.price} руб
                        </span>
                        <span
                            className="carts-container__item__delete"
                            onClick={() => cart.removeFromCart(item.pizza.id, item.size)}
                        >
                            <Icon name="delete" size={10} />
                        </span>
                    </li>
                ))}
            </ul>

            <div className="carts-container__summary">
                <p className="default-text">
                    Сумма заказа:
                    <span className="carts-container__summary__payment">
                        {cart.getTotalAmount()}
                    </span>{' '}
                    руб
                </p>
            </div>
        </div>
    );
};

export default withStateManager<unknown, InjectedProps>(CartItems, stores => ({
    cart: stores.cart,
}));
