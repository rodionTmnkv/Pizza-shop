import React from 'react';

//Импорт компонентов
import Modal from 'src/components/Modal';
import CartForm from './modules/CartForm';
import CartItems from './modules/CartItems';

//Импорт стилей
import './styles.sass';

type Props = {
    isOpen: boolean;
    onClose: () => void;
};

export const Cart = ({ isOpen, onClose }: Props): React.ReactElement => {
    // const useDisableScroll = () => {
    // useEffect(() => {
    //     const bodyElement = document.getElementsByTagName('body')[0];
    //     bodyElement.style.overflowY = 'hidden';
    //
    //     return () => {
    //         bodyElement.style.overflowY = 'scroll';
    //     }
    // }, [])}

    return (
        <Modal isOpen={isOpen} closeModal={onClose} title={'Ваш заказ'}>
            <CartItems />
            <CartForm onClose={onClose} />
        </Modal>
    );
};
