// Svg
import FooterLogo from '../assets/images/png/footer-logo.png';
// Images
import HeaderLogo from '../assets/images/png/header-logo.png';
import HeaderMobileLogo from '../assets/images/png/header-mobile-logo.png';
import MainBenefitsCooking from '../assets/images/png/main-benefits-cooking.png';
import MainBenefitsIngredients from '../assets/images/png/main-benefits-ingredients.png';
import MainBenefitsServices from '../assets/images/png/main-benefits-services.png';
import MainContent from '../assets/images/png/main-content.png';
import MainInst_1 from '../assets/images/png/main-inst01.png';
import MainInst_2 from '../assets/images/png/main-inst02.png';
import MainInst_3 from '../assets/images/png/main-inst03.png';
import MainInst_4 from '../assets/images/png/main-inst04.png';
import MainInst_5 from '../assets/images/png/main-inst05.png';
import MainInst_6 from '../assets/images/png/main-inst06.png';
import MainInst_7 from '../assets/images/png/main-inst07.png';
import MainInst_8 from '../assets/images/png/main-inst08.png';
import MainInst_9 from '../assets/images/png/main-inst09.png';
import MainInst_10 from '../assets/images/png/main-inst10.png';
import MainMenuPizza_1 from '../assets/images/png/main-menu-pizza_1.png';
import MainMenuPizza_2 from '../assets/images/png/main-menu-pizza_2.png';
import MainMenuPizza_3 from '../assets/images/png/main-menu-pizza_3.png';
import MainMenuPizza_4 from '../assets/images/png/main-menu-pizza_4.png';
import MainMenuPizza_5 from '../assets/images/png/main-menu-pizza_5.png';
import MainMenuPizza_6 from '../assets/images/png/main-menu-pizza_6.png';
import MainMenuPizza_7 from '../assets/images/png/main-menu-pizza_7.png';
import MainMenuPizza_8 from '../assets/images/png/main-menu-pizza_8.png';
import MainMenuPizza_9 from '../assets/images/png/main-menu-pizza_9.png';
import MainMenuPizza_10 from '../assets/images/png/main-menu-pizza_10.png';
import MainMenuPizza_11 from '../assets/images/png/main-menu-pizza_11.png';
import MainMenuPizza_12 from '../assets/images/png/main-menu-pizza_12.png';
import MainStockDiscount from '../assets/images/png/main-stock-discount.png';
import MainStockDrink from '../assets/images/png/main-stock-drink.png';
import MainStockPizza from '../assets/images/png/main-stock-pizza.png';
import HeaderCart from '../assets/images/svg/header-cart.svg';
import HeaderCloseMenu from '../assets/images/svg/header-close-menu.svg';
import HeaderOpenMenu from '../assets/images/svg/header-open-menu.svg';
import HeaderTelephone from '../assets/images/svg/header-telephone.svg';
import MainDeliveryDelivery from '../assets/images/svg/main-delivery-delivery.svg';
import MainDeliveryOrder from '../assets/images/svg/main-delivery-order.svg';
import MainDeliveryPayment from '../assets/images/svg/main-delivery-payment.svg';
import MainMenuCardAcute from '../assets/images/svg/main-menu-card-acute.svg';
import MainMenuCardAll from '../assets/images/svg/main-menu-card-all.svg';
import MainMenuCardCheese from '../assets/images/svg/main-menu-card-cheese.svg';
import MainMenuCardMeat from '../assets/images/svg/main-menu-card-meat.svg';
import MainMenuCardVegan from '../assets/images/svg/main-menu-card-vegan.svg';

export const Svg = {
    HeaderCart,
    HeaderTelephone,
    HeaderOpenMenu,
    HeaderCloseMenu,
    MainDeliveryOrder,
    MainDeliveryDelivery,
    MainDeliveryPayment,
    MainMenuCardAll,
    MainMenuCardAcute,
    MainMenuCardMeat,
    MainMenuCardCheese,
    MainMenuCardVegan,
};

export const Images = {
    HeaderLogo,
    HeaderMobileLogo,
    MainContent,
    MainStockPizza,
    MainStockDrink,
    MainStockDiscount,
    MainMenuPizza_1,
    MainMenuPizza_2,
    MainMenuPizza_3,
    MainMenuPizza_4,
    MainMenuPizza_5,
    MainMenuPizza_6,
    MainMenuPizza_7,
    MainMenuPizza_8,
    MainMenuPizza_9,
    MainMenuPizza_10,
    MainMenuPizza_11,
    MainMenuPizza_12,
    MainInst_1,
    MainInst_2,
    MainInst_3,
    MainInst_4,
    MainInst_5,
    MainInst_6,
    MainInst_7,
    MainInst_8,
    MainInst_9,
    MainInst_10,
    MainBenefitsCooking,
    MainBenefitsIngredients,
    MainBenefitsServices,
    FooterLogo,
};
