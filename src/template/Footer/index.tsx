//Импорт стилей
import './styles.sass';

import React from 'react';

//Импорт компонентов
import { Images } from '../../utils/images';

const Footer = (): React.ReactElement => {
    return (
        <footer className="footer">
            <div className="footer-container">
                <div className="footer-container__logo">
                    <img src={Images.FooterLogo} alt="" />
                </div>
                <span className="footer-container__phone">
                    <h4>
                        <a href="tel:+7 ( 918 ) 432 - 65 - 87">+7 ( 918 ) 432 - 65 - 87</a>
                    </h4>
                    <p className="footer-container__phone__description">
                        Ежедневно с 9:00 до 23:00
                    </p>
                </span>
            </div>
            <span className="footer-policy">
                <a href="/#">Политика конфиденциальности</a>
            </span>
        </footer>
    );
};

export default Footer;
