//Ипорт стилей
import './styles.sass';

import React from 'react';
//Импорт компонентов
import { Images } from 'src/utils/images';

const HeaderLogo = (): React.ReactElement => {
    return (
        <div className="header-logo">
            <img src={Images.HeaderLogo} alt="" />
        </div>
    );
};

export default HeaderLogo;
