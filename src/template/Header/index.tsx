//Ипорт стилей
import './styles.sass';

import React, { useEffect } from 'react';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

import HeaderContent from './HeaderContent';
//Импорт компонентов
import HeaderLogo from './HeaderLogo';
import HeaderNav from './HeaderNav';

type Props = {
    showCart: () => void;
};

type InjectedProps = Pick<IStore, 'cart'>;

const Header = ({ showCart }: Props & InjectedProps): React.ReactElement => {
    function scrollFunction() {
        if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
            document.getElementById('header')?.classList.add('header-scroll');
        } else {
            document.getElementById('header')?.classList.remove('header-scroll');
        }
    }

    useEffect(() => {
        window.onscroll = scrollFunction;
        return () => {
            window.onscroll = null;
        };
    }, []);

    return (
        <header className="header" id="header">
            <HeaderLogo />
            <HeaderNav />
            <HeaderContent showCart={showCart} />
        </header>
    );
};

export default withStateManager<Props, InjectedProps>(Header, stores => ({
    cart: stores.cart,
}));
