//Ипорт стилей
import './styles.sass';

import React from 'react';
//Импорт компонентов
import { Svg } from 'src/utils/images';

import { withStateManager } from '../../../hock/withStateManager';
import { IStore } from '../../../mobx/store';
import HeaderMobileNav from '../HeaderMobileNav';

type Props = {
    showCart: () => void;
};

type InjectedProps = Pick<IStore, 'cart'>;

const HeaderContent = ({ showCart, cart }: Props & InjectedProps): React.ReactElement => {
    return (
        <div className="header-container">
            <div className="header-phone">
                <img className="header-phone__image" src={Svg.HeaderTelephone} alt="" />
                <span className="header-phone__contacts">
                    <p className="header-phone__contacts__tel">
                        <a href="tel:+7 ( 918 ) 432 - 65 - 87">+7 ( 918 ) 432 - 65 - 87</a>
                    </p>
                    <p className="description">Ежедневно с 9:00 до 23:00</p>
                </span>
            </div>

            <div className="header-order" onClick={showCart}>
                <span className="header-order__counter">{cart.getItemsCount()}</span>
                <img src={Svg.HeaderCart} alt="" />
                <span className="header-order__info">
                    <span className="header-order__title">Ваш заказ</span>
                    <p className="description">{cart.getOrderName()}</p>
                </span>
            </div>
            <button className="header-lang">EN</button>
            <HeaderMobileNav />
        </div>
    );
};

export default withStateManager<Props, InjectedProps>(HeaderContent, stores => ({
    cart: stores.cart,
}));
