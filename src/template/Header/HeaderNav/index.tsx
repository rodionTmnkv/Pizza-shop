import React, { useState } from 'react';
import { Link } from 'react-scroll';

//Ипорт стилей
import './styles.sass';

const HeaderNav = (): React.ReactElement => {
    const [visibility, setVisibility] = useState(false);

    return (
        <ul
            className={visibility ? 'header-nav open' : 'header-nav'}
            id="header-navbar"
            onClick={() => setVisibility(false)}
        >
            <Link to="menu" duration={300}>
                <li className="header-nav__item">Меню</li>
            </Link>
            <Link to="benefits" duration={200}>
                <li className="header-nav__item">О нас</li>
            </Link>
            <Link to="inst" duration={200}>
                <li className="header-nav__item">Контакты</li>
            </Link>
        </ul>
    );
};

export default HeaderNav;
