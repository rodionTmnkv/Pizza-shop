//Ипорт стилей
import './styles.sass';

import React, { useState } from 'react';
import { Link } from 'react-scroll';

//Импорт компонентов
import { Images, Svg } from '../../../utils/images';

const HeaderMobileNav = (): React.ReactElement => {
    const body = document.getElementsByTagName('body')[0];
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const openMenu = () => {
        body.style.overflow = 'hidden';
        setIsMenuOpen(true);
    };

    const hideMenu = () => {
        body.attributes.removeNamedItem('style');
        setIsMenuOpen(false);
    };

    return (
        <div className="header-mobile__nav">
            <div className={`header-mobile__nav__menu ${isMenuOpen ? 'active' : ''}`}>
                <div className="header-mobile__container">
                    <div className="header-mobile__nav__title">
                        <img
                            className="header-mobile__nav__logo"
                            src={Images.HeaderMobileLogo}
                            alt=""
                        />
                        <img src={Svg.HeaderCloseMenu} alt="" onClick={hideMenu} />
                    </div>
                    <ul className="header-mobile__nav__list">
                        <li className="header-mobile__nav__link">
                            <Link to="menu" duration={300}>
                                Меню
                            </Link>
                        </li>

                        <li className="header-mobile__nav__link">
                            <Link to="benefits" duration={200}>
                                О нас
                            </Link>
                        </li>
                        <li className="header-mobile__nav__link">
                            <Link to="inst" duration={200}>
                                Контакты
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="header-mobile__nav__contacts">
                    <p className="header-mobile__nav__contacts__text">Заказать по телефону</p>
                    <h3>+7 (918) 432-65-87</h3>
                    <p className="description">Ежедневно с 9:00 до 23:00</p>
                    <p className="header-mobile__nav__lang">English</p>
                </div>
            </div>
            <button className="header-mobile__nav__button" onClick={openMenu}>
                <img src={Svg.HeaderOpenMenu} alt="" />
            </button>
        </div>
    );
};

export default HeaderMobileNav;
