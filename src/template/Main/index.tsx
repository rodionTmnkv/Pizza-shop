import React from 'react';

//Импорт компонентов
import MainContent from './modules/MainContent';
import Stocks from '../../components/Stocks';
// import MainStocks from "./modules/MainStocks";
// import StocksSlider from "../../components/StocksSlider";
import MainMenu from './modules/MainMenu';
import MainDelivery from './modules/MainDelivery';
import MainBenefits from './modules/MainBenefits';
import MainInst from './modules/MainInst';

//Импорт стилей
import './styles.sass';

const Main = (): React.ReactElement => {
    return (
        <main className="main">
            <MainContent />
            <Stocks />
            <MainMenu />
            <MainDelivery />
            <MainBenefits />
            <MainInst />
        </main>
    );
};

export default Main;
