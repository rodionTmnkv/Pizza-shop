//Импорт стилей
import './styles.sass';

import React from 'react';

//Импорт компонентов
import { Images } from '../../../../utils/images';

const MainInst = (): React.ReactElement => {
    return (
        <section className="main-inst" id="inst">
            <h2>Следите за нами в Instagram</h2>
            <h4>
                <a href="src/template/Main/modules/MainInst/index#">@pizzamenu</a>
            </h4>
            <div className="main-inst__pictures">
                <div>
                    <img src={Images.MainInst_1} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_2} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_3} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_4} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_5} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_6} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_7} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_8} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_9} alt="" />
                </div>
                <div>
                    <img src={Images.MainInst_10} alt="" />
                </div>
            </div>
        </section>
    );
};

export default MainInst;
