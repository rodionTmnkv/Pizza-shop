//Импорт стилей
import './styles.sass';

import React from 'react';

//Импорт компонентов
import MainPizzasList from './modules/MainPizzasList';
import MainPizzaTypes from './modules/MainPizzaTypes';

const MainMenu = (): React.ReactElement => {
    return (
        <section className="main-menu" id="menu">
            <MainPizzaTypes />
            <MainPizzasList />
        </section>
    );
};

export default MainMenu;
