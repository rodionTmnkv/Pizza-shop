import React from 'react';
import { IStore } from 'src/mobx/store';
import { withStateManager } from 'src/hock/withStateManager';

//Импорт компонентов
import MainPizzaCard from './modules/MainPizzaCard';

//Импорт стилей
import './styles.sass';

type InjectedProps = Pick<IStore, 'pizzas'>;

const MainPizzasList = ({ pizzas }: InjectedProps): React.ReactElement => {
    return (
        <ul className="main-menu__cards">
            {pizzas.map(pizza => (
                <MainPizzaCard key={pizza.id} pizza={pizza} />
            ))}
        </ul>
    );
};

export default withStateManager<unknown, InjectedProps>(MainPizzasList, stores => ({
    pizzas: stores.pizzas.filter(
        p =>
            stores.pizzaFilter.pizzaTypeId == null ||
            p.types.find(t => t.id === stores.pizzaFilter.pizzaTypeId)
    ),
}));
