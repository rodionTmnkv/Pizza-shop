// Импорт стилей
import './styles.sass';

import React, { useState } from 'react';
// Импорт компонентов
import { PizzaSize } from 'src/data/Enums/PizzaSize';
import { PizzaModel } from 'src/data/Models/PizzaModel';
import { withStateManager } from 'src/hock/withStateManager';
import { IStore } from 'src/mobx/store';

type Props = {
    pizza: PizzaModel;
};

type InjectedProps = Pick<IStore, 'cart'>;

function MainPizzaCard({ pizza, cart }: Props & InjectedProps): React.ReactElement {
    const [selectedSize, setSelectedSize] = useState(PizzaSize.Medium);

    const selectedPrice = pizza.prices.find(price => price.size === selectedSize)?.price;

    const getPizzaImageClassName = () => {
        switch (selectedSize) {
            case PizzaSize.Small:
                return 'small';

            case PizzaSize.Medium:
                return 'medium';

            case PizzaSize.Large:
                return 'large';
        }
    }; // getPizzaImageClassName

    const handleSizeSelecting = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedSize(Number(e.target.value) as PizzaSize);
    }; // handleSizeSelecting

    const handleOrdering = () => {
        cart.addToCart(pizza, selectedSize);
    }; // handleOrdering

    return (
        <li className="menu-card">
            <div className="menu-card__image__wrapper">
                <div className="menu-card__type__images">
                    {pizza.types.map(type => (
                        <img key={type.id} src={type.image} alt={type.name} />
                    ))}
                </div>

                <div className="menu-card__image">
                    <img src={pizza.image} alt={pizza.name} className={getPizzaImageClassName()} />
                </div>
            </div>

            <div className="menu-card__content__wrapper">
                <h4 className="menu-card__title">{pizza.name}</h4>
                <p className="description">{pizza.description}</p>

                <span className="description-size">Размер, см</span>
                <div className="menu-card__size__buttons">
                    {pizza.prices.map(price => (
                        <label
                            key={price.id}
                            className={`menu-card__size__label ${
                                selectedSize === price.size ? 'active' : ''
                            }`}
                        >
                            <input
                                className="menu-card__size__button"
                                value={price.size}
                                type="radio"
                                checked={selectedSize === price.size}
                                onChange={handleSizeSelecting}
                            />

                            {price.size}
                        </label>
                    ))}
                </div>

                <span className="menu-card__price">от {selectedPrice} руб</span>

                <button className="menu-card__button" onClick={handleOrdering}>
                    <span className="menu-card__button__price">от {selectedPrice} руб</span>
                    <span className="menu-cart__button__text">Заказать</span>
                </button>
            </div>
        </li>
    );
}

export default withStateManager<Props, InjectedProps>(MainPizzaCard, stores => ({
    cart: stores.cart,
}));
