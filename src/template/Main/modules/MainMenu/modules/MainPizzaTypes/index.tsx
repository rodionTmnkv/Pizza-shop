// Импорт стилей
import './styles.sass';

import { IPizzaFilterModel } from '@src/data/Models/PizzaFilterModel';
import { IPizzaTypeModel } from '@src/data/Models/PizzaTypeModel';
import { FC } from 'react';
import { withStateManager } from 'src/hock/withStateManager';
// Импорт компонентов
import { Svg } from 'src/utils/images';

// type InjectedProps = Pick<IStore, 'pizzaTypes' | 'pizzaFilter'>;

interface InjectedProps {
    pizzaTypes: IPizzaTypeModel[];
    pizzaFilter: IPizzaFilterModel;
}

const MainPizzaTypes: FC<InjectedProps> = ({ pizzaTypes, pizzaFilter }) => {
    return (
        <>
            <h2>Выберите пиццу</h2>
            <ul className="main-menu__nav">
                <li onClick={() => pizzaFilter?.setPizzaTypeId(null)}>
                    <span className="main-menu__nav__list">Все</span>
                    <img src={Svg.MainMenuCardAll} alt="" />
                </li>

                {pizzaTypes.map(pizzaType => (
                    <li key={pizzaType.id} onClick={() => pizzaFilter.setPizzaTypeId(pizzaType.id)}>
                        <img src={pizzaType.image} alt="" />
                        <span className="main-menu__nav__list">{pizzaType.name}</span>
                    </li>
                ))}
            </ul>
        </>
    );
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default withStateManager<unknown, InjectedProps>(MainPizzaTypes, stores => ({
    pizzaTypes: stores.pizzaTypes,
    pizzaFilter: stores.pizzaFilter,
}));
