//Импорт стилей
import './styles.sass';

import React from 'react';
import { Link } from 'react-scroll';
//Импорт компонентов
import { Images } from 'src/utils/images';

const MainContent = (): React.ReactElement => {
    return (
        <section className="main-content">
            <div className="main-content__title">
                <h1>Пицца на заказ</h1>
                <p className="accent">
                    Бесплатная и быстрая доставка за час в любое удобное для вас время
                </p>
                <Link to="menu" duration={400}>
                    <button className="main-content__button">Выбрать пиццу</button>
                </Link>
            </div>
            <div className="main-content__image">
                <img src={Images.MainContent} alt="" />
            </div>
        </section>
    );
};

export default MainContent;
