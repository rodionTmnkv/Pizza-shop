//Импорт стилей
import './styles.sass';

import React from 'react';

//Импорт компонентов
import { Images } from '../../../../utils/images';

//GetData

const MainStocks = (): React.ReactElement => {
    return (
        <section className="main-stocks">
            <div className="main-stocks__container">
                <img src={Images.MainStockPizza} alt="" />
                <h4>Закажи 2 пиццы – 3-я в подарок</h4>
                <p className="main-stocks__container__text">
                    При заказе 2-х больших пицц – средняя пицца в подарок
                </p>
            </div>
            <div className="main-stocks__container">
                <img src={Images.MainStockDrink} alt="" />
                <h4>Напиток в подарок</h4>
                <p className="main-stocks__container__text">
                    Скидка на заказ от 3 000 рублей + напиток в подарок
                </p>
            </div>

            <div className="main-stocks__container">
                <img src={Images.MainStockDiscount} alt="" />
                <h4>25% при первом заказе</h4>
                <p>Скидка новым клиентам!</p>
            </div>
        </section>
    );
};

export default MainStocks;
