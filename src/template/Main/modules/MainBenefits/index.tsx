//Импорт стилей
import './styles.sass';

import React from 'react';

//Импорт компонентов
import { Images } from '../../../../utils/images';

const MainBenefits = (): React.ReactElement => {
    return (
        <section className="main-benefits" id="benefits">
            <div className="main-benefits__container">
                <img src={Images.MainBenefitsCooking} alt="" />
                <span className="main-benefits__container__info">
                    <h3>Изготавливаем пиццу по своим рецептам в лучших традициях</h3>
                    <p>
                        Наша пицца получается сочной, вкусной и главное хрустящей с нежной и
                        аппетитной начинкой, готовим по своим итальянским рецептам
                    </p>
                </span>
            </div>

            <div className="main-benefits__container">
                <span className="main-benefits__container__info">
                    <h3>Используем только свежие ингридиенты</h3>
                    <p>
                        Ежедневно заготавливаем продукты и овощи для наших пицц, соблюдаем все сроки
                        хранения
                    </p>
                </span>
                <img src={Images.MainBenefitsIngredients} alt="" />
            </div>

            <div className="main-benefits__container">
                <img src={Images.MainBenefitsServices} alt="" />
                <span className="main-benefits__container__info">
                    <h3>Изготавливаем пиццу по своим рецептам в лучших традициях</h3>
                    <p>
                        Наша пицца получается сочной, вкусной и главное хрустящей с нежной и
                        аппетитной начинкой, готовим по своим итальянским рецептам
                    </p>
                </span>
            </div>
        </section>
    );
};

export default MainBenefits;
