//Импорт стилей
import './styles.sass';

import { observer } from 'mobx-react';
import React from 'react';

//Импорт компонентов
import { Svg } from '../../../../utils/images';

const MainDelivery = (): React.ReactElement => {
    return (
        <section className="main-delivery">
            <h2>Доставка и оплата</h2>
            <div className="main-delivery__wrapper">
                <div className="main-delivery__container">
                    <img src={Svg.MainDeliveryOrder} alt="" />
                    <span className="main-delivery__container__services">
                        <h4>Заказ</h4>
                        <p className="main-delivery__container__text">
                            После оформления заказа мы свяжемся с вами для уточнения деталей.
                        </p>
                    </span>
                </div>
                <div className="main-delivery__container">
                    <img src={Svg.MainDeliveryDelivery} alt="" />
                    <span className="main-delivery__container__services">
                        <h4>Доставка курьером</h4>
                        <p className="main-delivery__container__text">
                            Мы доставим вашу пиццу горячей. Бесплатная доставка по городу.
                        </p>
                    </span>
                </div>
                <div className="main-delivery__container">
                    <img src={Svg.MainDeliveryPayment} alt="" />
                    <span className="main-delivery__container__services">
                        <h4>Оплата</h4>
                        <p className="main-delivery__container__text">
                            Оплатить можно наличными или картой курьеру. И золотом тоже можно.
                        </p>
                    </span>
                </div>
            </div>
        </section>
    );
};

export default observer(MainDelivery);
