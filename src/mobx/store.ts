import { inject } from 'mobx-react';
import { CartModel } from 'src/data/Models/CartModel';
import { PizzaFilterModel } from 'src/data/Models/PizzaFilterModel';
import { PizzaModel } from 'src/data/Models/PizzaModel';
import { PizzaTypeModel } from 'src/data/Models/PizzaTypeModel';
import { pizzasMock } from 'src/mock/PizzasMock';
import { pizzaTypesMock } from 'src/mock/PizzaTypesMock';

export interface IStore {
    pizzas: PizzaModel[];
    pizzaTypes: PizzaTypeModel[];
    pizzaFilter: PizzaFilterModel;
    cart: CartModel;
}

export const store: IStore = {
    pizzas: pizzasMock,
    pizzaTypes: pizzaTypesMock,
    pizzaFilter: new PizzaFilterModel(null),
    cart: new CartModel([]),
};

export const typedInject = (key: keyof IStore) => {
    return inject(key);
};
