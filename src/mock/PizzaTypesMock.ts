import { PizzaTypeModel } from 'src/data/Models/PizzaTypeModel';

import { Svg } from '../utils/images';

export const pizzaTypesMock: PizzaTypeModel[] = [
    {
        id: 1,
        name: 'Острые',
        image: Svg.MainMenuCardAcute,
    },
    {
        id: 2,
        name: 'Мясные',
        image: Svg.MainMenuCardMeat,
    },
    {
        id: 3,
        name: 'Сырные',
        image: Svg.MainMenuCardCheese,
    },
    {
        id: 4,
        name: 'Веганские',
        image: Svg.MainMenuCardVegan,
    },
];
