import { PizzaModel } from '@src/data/Models/PizzaModel';
import { PizzaSize } from 'src/data/Enums/PizzaSize';
import { Images } from 'src/utils/images';

import { pizzaTypesMock } from './PizzaTypesMock';

export const pizzasMock: PizzaModel[] = [
    {
        id: 1,
        name: 'Итальянская',
        image: Images.MainMenuPizza_1,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[0]],
    },
    {
        id: 2,
        name: 'Маргарита',
        image: Images.MainMenuPizza_2,
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
    {
        id: 3,
        name: 'Барбекю',
        image: Images.MainMenuPizza_3,
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
    {
        id: 4,
        name: 'Вегетарианская',
        image: Images.MainMenuPizza_4,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[3]],
    },
    {
        id: 5,
        name: 'Мясная',
        image: Images.MainMenuPizza_5,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1], pizzaTypesMock[0]],
    },
    {
        id: 6,
        name: 'Овощная',
        image: Images.MainMenuPizza_6,
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[3]],
    },
    {
        id: 7,
        name: 'Римская',
        image: Images.MainMenuPizza_7,
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[0]],
    },
    {
        id: 8,
        name: 'С грибами',
        image: Images.MainMenuPizza_8,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 9,
        name: 'Сырная',
        image: Images.MainMenuPizza_9,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 10,
        name: 'Четыре сыра',
        image: Images.MainMenuPizza_10,
        description: 'Тесто со шпинатом, молодой сыр и колбаски, много колбасок',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[2]],
    },
    {
        id: 11,
        name: 'Пепперони Фреш с томатами',
        image: Images.MainMenuPizza_11,
        description: 'Циплёнок (маленький кура), оливки, моцарелла, соус барбекю',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [],
    },
    {
        id: 12,
        name: 'Ветчина и сыр',
        image: Images.MainMenuPizza_12,
        description: 'Томат, шампиньон, сыр, оливки, чили, соус, тесто, базилик',
        prices: [
            {
                id: 1,
                size: PizzaSize.Small,
                price: 399,
            },
            {
                id: 2,
                size: PizzaSize.Medium,
                price: 479,
            },
            {
                id: 3,
                size: PizzaSize.Large,
                price: 699,
            },
        ],
        types: [pizzaTypesMock[1]],
    },
];
