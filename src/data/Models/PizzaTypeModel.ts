import { makeAutoObservable } from 'mobx';

export interface IPizzaTypeModel {
    id: number;
    name: string;
    image: string;
}

export class PizzaTypeModel implements IPizzaTypeModel {
    public id: number;

    public name: string;

    public image: string;

    constructor(id: number, name: string, image: string) {
        makeAutoObservable(this);

        this.id = id;
        this.name = name;
        this.image = image;
    }
}
