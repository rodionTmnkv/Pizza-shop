import { makeAutoObservable } from 'mobx';
import { PizzaSize } from '../Enums/PizzaSize';
import { PizzaModel } from './PizzaModel';

export class CartModel {
    constructor(
        public items: {
            pizza: PizzaModel;
            size: PizzaSize;
            price: number;
            count: number;
            amount: number;
        }[]
    ) {
        makeAutoObservable(this);
    }

    public addToCart(pizza: PizzaModel, size: PizzaSize) {
        const pizzaInCart = this.items.find(i => i.pizza.id === pizza.id && i.size === size);
        if (pizzaInCart) {
            pizzaInCart.count++;
            pizzaInCart.amount += pizzaInCart.price;
            return;
        } // if

        const pizzaPrice = pizza.prices.find(p => p.size === size)?.price;
        if (pizzaPrice) {
            this.items.push({
                pizza: pizza,
                size: size,
                count: 1,
                price: pizzaPrice,
                amount: pizzaPrice,
            });
        } // if
    } // addToCart

    public increaseCount(pizzaId: number, size: PizzaSize) {
        const pizzaInCart = this.items.find(i => i.pizza.id === pizzaId && i.size === size);
        if (pizzaInCart) {
            pizzaInCart.count++;
            pizzaInCart.amount += pizzaInCart.price;
        } // if
    } // increaseCount

    public decreaseCount(pizzaId: number, size: PizzaSize) {
        const pizzaInCart = this.items.find(i => i.pizza.id === pizzaId && i.size === size);
        if (pizzaInCart) {
            pizzaInCart.count--;
            pizzaInCart.amount -= pizzaInCart.price;
        } // if
    } // decreaseCount

    public removeFromCart(pizzaId: number, size: PizzaSize) {
        this.items = this.items.filter(i => !(i.pizza.id === pizzaId && i.size === size));
    } // removeFromCart

    public clearCart() {
        this.items = [];
    } // clearCart

    public getOrderName() {
        const pizzasCount = this.getItemsCount();
        if (!pizzasCount) {
            return 'Название заказа';
        }
        return `${this.items[0].pizza.name} и ещё ${pizzasCount - 1} пицц(ы)`;
    }

    public getTotalAmount() {
        return this.items.reduce((prev, curr) => (prev += curr.amount), 0);
    } // getTotalAmount

    public getItemsCount() {
        return this.items.reduce((prev, curr) => {
            prev += curr.count;
            return prev;
        }, 0);
    } // getItemsCount
}
