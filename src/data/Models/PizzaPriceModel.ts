import { makeAutoObservable } from 'mobx';
import { PizzaSize } from '../Enums/PizzaSize';

export class PizzaPriceModel {
    constructor(public id: number, public size: PizzaSize, public price: number) {
        makeAutoObservable(this);
    } // constructor
}
