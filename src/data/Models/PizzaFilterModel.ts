import { makeAutoObservable } from 'mobx';

export interface IPizzaFilterModel {
    pizzaTypeId: number | null;
    setPizzaTypeId: (pizzaTypeId: number | null) => void;
}

export class PizzaFilterModel implements IPizzaFilterModel {
    pizzaTypeId: number | null;

    constructor(pizzaTypeId: number | null) {
        this.pizzaTypeId = pizzaTypeId;

        makeAutoObservable(this);
    } // PizzaFilterModel

    setPizzaTypeId(pizzaTypeId: number | null) {
        this.pizzaTypeId = pizzaTypeId;
    } // setPizzaTypeId
}
