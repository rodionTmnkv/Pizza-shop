import { makeAutoObservable } from 'mobx';
import { PizzaPriceModel } from './PizzaPriceModel';
import { PizzaTypeModel } from './PizzaTypeModel';

export class PizzaModel {
    constructor(
        public id: number,
        public name: string,
        public image: string,
        public description: string,
        public prices: PizzaPriceModel[],
        public types: PizzaTypeModel[]
    ) {
        makeAutoObservable(this);
    } // constructor
}
