declare module '*.scss' {
    const content: { [className: string]: string };
    export = content;
}

declare module '*.sass' {
    const content: { [className: string]: string };
    export = content;
}

declare module '*css' {
    const content: { [className: string]: string };
    export = content;
}
declare module '*.jpg';
declare module '*.png';
declare module '*.svg';
declare module '*.webp';
declare module '*.json';
